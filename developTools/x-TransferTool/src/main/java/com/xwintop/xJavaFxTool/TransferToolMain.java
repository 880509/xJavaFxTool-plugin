package com.xwintop.xJavaFxTool;

import com.xwintop.xcore.util.javafx.FxmlUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import java.util.ResourceBundle;

@Slf4j
public class TransferToolMain extends Application {

    private static final String FXML_PATH = "/com/xwintop/xJavaFxTool/fxmlView/developTools/xTransferTool/TransferTool.fxml";

    private static final String BUNDLE_NAME = "locale.TransferTool";

    public static void main(String[] args) {
        try {
            launch(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
        FXMLLoader fXMLLoader = FxmlUtil.loadFxmlFromResource(FXML_PATH, resourceBundle);

        primaryStage.setResizable(true);
        primaryStage.setTitle(resourceBundle.getString("Title"));
        primaryStage.setScene(new Scene(fXMLLoader.getRoot()));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

}